package br.open.system.teste.java.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.open.system.teste.java.dao.DAO;
import br.open.system.teste.java.entidade.Fornecedor;


@Path("/fornecedores")
public class FornecedorRestServer {

	// funções reste para Fornecedors
	static private Map<Integer, Fornecedor> fornecedorsMap;

	static {

		List<Fornecedor> Fornecedors;
		DAO<Fornecedor> doDao;
		doDao = new DAO<>(Fornecedor.class);
		Fornecedors = new ArrayList<>();
		fornecedorsMap = new HashMap<Integer, Fornecedor>();

		Fornecedors = doDao.listaTodos();

		for (Fornecedor Fornecedor : Fornecedors) {
			fornecedorsMap.put(Fornecedor.getIdPessoa(), Fornecedor);
		}
	}

	/*****************************************************************
	 * @author erison.santos Captura por rest a liste de clitentes cadastrado no
	 *         banco.
	 * @return liste de Fornecedors
	 *****************************************************************/
	@GET
	@Produces("text/xml")
	@Consumes(MediaType.APPLICATION_XML)
	//@Path("/getFornecedors")
	public List<Fornecedor> getFornecedors() {
		return new ArrayList<Fornecedor>(fornecedorsMap.values());
	}

	@POST
	@Path("/post")
	@Consumes("application/json")
	public Response createProductInJSON(Fornecedor fornecedor) {

		String result = "Product created : " + fornecedor;
		return Response.status(201).entity(result).build();

	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@Path("{idPessoa}")
	@GET
	@Consumes(MediaType.APPLICATION_XML)
	@Produces("text/xml")
	public Fornecedor getFornecedor(@PathParam("idPessoa") int idPessoa) {
		//FornecedorRestUtil.getFornecedor(idPessoa);
		return fornecedorsMap.get(idPessoa);
	}

	@POST
	// @Consumes("text/xml")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces("text/plain")
	@Path("/cadastrar")
	public String adicionaFornecedor(Fornecedor fornecedor) {
		fornecedor.setIdPessoa(fornecedorsMap.size() + 1);
		fornecedorsMap.put(fornecedor.getIdPessoa(), fornecedor);
		return fornecedor.getNome() + " adicionado.";
	}

	@Path("{idPessoa}")
	@PUT
	@Consumes("text/xml")
	@Produces("text/plain")
	public String atualizaFornecedor(Fornecedor Fornecedor, @PathParam("idPessoa") int idPessoa) {
		Fornecedor atual = fornecedorsMap.get(idPessoa);
		atual.setNome(Fornecedor.getNome());
		atual.setEndereco(Fornecedor.getEndereco());
		return Fornecedor.getNome() + " atualizada.";
	}

	@Path("{idPessoa}")
	@DELETE
	@Produces("text/plain")
	public String removeFornecedor(@PathParam("idPessoa") int idPessoa) {
		fornecedorsMap.remove(idPessoa);
		return "Fornecedor removida.";
	}
}
