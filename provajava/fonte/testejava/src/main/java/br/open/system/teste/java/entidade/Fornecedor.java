package br.open.system.teste.java.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "fornecedor", schema = "testejava")
@PrimaryKeyJoinColumn(name = "idPessoa")
@Inheritance(strategy = InheritanceType.JOINED)
@XmlRootElement
public class Fornecedor extends Master{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7893277916977765515L;
	
	@Column(name="cnpj" )
	private String cnpj;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

}
