package br.open.system.teste.java.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.open.system.teste.java.dao.DAO;
import br.open.system.teste.java.entidade.Cliente;

@Path("/clientes")
public class ClienteRestServer {

	// funções reste para clientes
	static private Map<Integer, Cliente> clientesMap;

	static {

		List<Cliente> clientes;
		DAO<Cliente> doDao;
		doDao = new DAO<>(Cliente.class);
		clientes = new ArrayList<>();
		clientesMap = new HashMap<Integer, Cliente>();

		clientes = doDao.listaTodos();

		for (Cliente cliente : clientes) {
			clientesMap.put(cliente.getIdPessoa(), cliente);
		}
	}

	/*****************************************************************
	 * @author erison.santos Captura por rest a lista de clitentes cadastrado no
	 *         banco.
	 * @return liste de clientes
	 *****************************************************************/
	@GET
	@Produces("text/xml")
	@Consumes(MediaType.APPLICATION_XML)
	public List<Cliente> getClientes() {
		return new ArrayList<Cliente>(clientesMap.values());
	}

	/*****************************************************************
	 * @author erison.santos 
		cadas
	 * @return liste de clientes
	 *****************************************************************/
	@POST
	@Path("/post")
	@Consumes("application/json")
	public Response createProductInJSON(Cliente cliente) {

		String result = "Product created : " + cliente;
		return Response.status(201).entity(result).build();

	}

	/**
	 * Esse método pega um cliente específico
	 * @param id
	 * @return
	 */
	@Path("{idPessoa}")
	@GET
	@Consumes(MediaType.APPLICATION_XML)
	@Produces("text/xml")
	public Cliente getCliente(@PathParam("idPessoa") int idPessoa) {
		return clientesMap.get(idPessoa);
	}

	@POST
	// @Consumes("text/xml")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces("text/plain")
	@Path("/cadastrar")
	public String adicionaCliente(Cliente cliente) {
		cliente.setIdPessoa(clientesMap.size() + 1);
		clientesMap.put(cliente.getIdPessoa(), cliente);
		return cliente.getNome() + " adicionado.";
	}

	@Path("{idPessoa}")
	@PUT
	@Consumes("text/xml")
	@Produces("text/plain")
	public String atualizaCliente(Cliente cliente, @PathParam("idPessoa") int idPessoa) {
		Cliente atual = clientesMap.get(idPessoa);
		atual.setNome(cliente.getNome());
		atual.setEndereco(cliente.getEndereco());
		return cliente.getNome() + " atualizada.";
	}

	@Path("{idPessoa}")
	@DELETE
	@Produces("text/plain")
	public String removeCliente(@PathParam("idPessoa") int idPessoa) {
		clientesMap.remove(idPessoa);
		return "Banda removida.";
	}
}
