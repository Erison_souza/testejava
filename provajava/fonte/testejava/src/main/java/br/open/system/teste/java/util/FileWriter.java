package br.open.system.teste.java.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileWriter {
	
	private InputStream inputStream;
	
	public FileWriter(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	/**
	 * Salva o arquivo
	 * @param caminhoDoArquivo
	 */
	public void save(String caminhoDoArquivo) {
		File file = new File(caminhoDoArquivo);
        FileOutputStream fileOutputStream = null;
		
		InputStreamReader InputStreamReader = new InputStreamReader(this.inputStream);
        BufferedReader reader = new BufferedReader(InputStreamReader);
        
        try {
        	caminhoDoArquivo = reader.readLine();
		} catch (IOException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
        
        try {
			fileOutputStream = new FileOutputStream(file);
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
        
        final int bufferSize = 4096; // buffer de 4KB
        byte[] buffer = new byte[bufferSize];
        int readedByte = -1;
        try {
			while ((readedByte = inputStream.read(buffer, 0, bufferSize)) != -1) {
			    try {
					fileOutputStream.write(buffer, 0, readedByte);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			fileOutputStream.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
