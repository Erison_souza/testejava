package br.open.system.teste.java.mb;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

@ManagedBean
@SessionScoped
public class HomeMB {

	private static String URL_HOME = "/index.xhtml?faces-redirect=true";
	private static String URL_REST_CLIENTE = "/rest/clientes";
	private static String URL_REST_FORNECEDOR = "/rest/fornecedores";

	/**
	 * Recupera a página inicial do sistema
	 * 
	 * @return
	 */
	
	/**
	 * Recupera a lista de clientes por rest
	 * 
	 * @return link para a lista de clientes
	 */
	private String linkClientesRest;
	/**
	 * Recupera a lista de forncedores por rest
	 * 
	 * @return link para a lista de fornecedores
	 */
	private String linkFornecedoresRest;
	
	public String paginaInicial() {

		return URL_HOME;
	}

	
	
	public String getLinkClientesRest() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String url = req.getRequestURL().toString();
		linkClientesRest = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath() + URL_REST_CLIENTE;
		 
		return linkClientesRest;
	}

	public void setLinkClientesRest(String linkClientesReste) {
		this.linkClientesRest = linkClientesReste;
	}



	public String getLinkFornecedoresRest() {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		String url = req.getRequestURL().toString();
		linkFornecedoresRest = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath()
				 +URL_REST_FORNECEDOR;
		return linkFornecedoresRest;
	}



	public void setLinkFornecedoresRest(String linkFornecedoresRest) {
		this.linkFornecedoresRest = linkFornecedoresRest;
	}

	
}
