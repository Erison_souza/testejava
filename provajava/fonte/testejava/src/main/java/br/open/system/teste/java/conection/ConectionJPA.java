package br.open.system.teste.java.conection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConectionJPA {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Postgres");
	
	
	
	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public void close(EntityManager em) {
		em.close();
	}
}
