package br.open.system.teste.java.mb;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.primefaces.model.UploadedFile;

import br.open.system.teste.java.util.FileWriter;

@ManagedBean
@SessionScoped
public class EnviarFotoMB {

	/**
	 * DEFINIÇÃO DAS URLS DA PÁGINA
	 */
	private static String URL_ENVIAR_FOTO = "/pages/upload-file" + ".xhtml?faces-redirect=true";

	private UploadedFile file;
	private FileWriter fileWriter;

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	/**
	 * Fazer upload do da foto
	 */
	public void upload() {
		if (file != null) {
			try {
				fileWriter = new FileWriter(file.getInputstream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String caminho = getRealPath() + "uploads\\" + file.getFileName();
			System.out.println(caminho);
			fileWriter.save(caminho);
			FacesMessage message = new FacesMessage(
					"Sucesso" + file.getFileName() + " foi enviado para o caminho:" + caminho,
					file.getFileName() + " foi enviado para o caminho:" + caminho);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public String telaEnviarFoto() {
		return URL_ENVIAR_FOTO;
	}

	/**
	 * Caminho real
	 * 
	 * @return
	 */

	public static String getRealPath() {

		FacesContext aFacesContext = FacesContext.getCurrentInstance();
		ServletContext context = (ServletContext) aFacesContext.getExternalContext().getContext();

		return context.getRealPath("/");
	}

}
