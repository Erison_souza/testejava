package br.open.system.teste.java.util;

import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

public class MensagensUtil {

	/**
	 * Esse método serve para capturar mensagens do arquivo
	 * messages.proproperties.
	 * 
	 * @param mensagem
	 * @return mensagem
	 */
	public static String getMessagem(String mensagem) {
		FacesContext context = FacesContext.getCurrentInstance();
		ResourceBundle.getBundle("/messages", context.getViewRoot().getLocale());
		ResourceBundle bundle = context.getApplication().getResourceBundle(context, "msg");
		return bundle.getString(mensagem);
	}
}
