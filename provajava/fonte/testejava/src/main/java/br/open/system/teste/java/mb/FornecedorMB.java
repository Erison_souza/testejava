package br.open.system.teste.java.mb;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.open.system.teste.java.dao.DAO;
import br.open.system.teste.java.entidade.Cliente;
import br.open.system.teste.java.entidade.Fornecedor;
import br.open.system.teste.java.util.MensagensUtil;

/**
 * Ess classe é a classe controller das telas de fornecedor.
 * 
 * @author erison
 *
 */
@ManagedBean
@SessionScoped
public class FornecedorMB implements InterfaceMB {

	/**
	 * DEFINIÇÃO DAS URLS DA PÁGINA
	 */
	private static String URL_CADASTRO = "/pages/fornecedor/mvc/new-provider.xhtml?faces-redirect=true";
	private static String URL_LISTAR = "/pages/fornecedor/mvc/list-provider.xhtml?faces-redirect=true";
	private static String URL_EDITAR = "/pages/fornecedor/mvc/edit-provider.xhtml?faces-redirect=true";
	/***********************
	 * VARIÁVEIS
	 **********************/

	private Fornecedor fornecedor;
	private List<Fornecedor> fornecedores;
	private DAO<Fornecedor> doDao;

	/**
	 * Construtor da classe
	 */
	public FornecedorMB() {
		super();
		doDao = new DAO<>(Fornecedor.class);
		this.fornecedor = new Fornecedor();
		fornecedores = new ArrayList<>();
	}

	/**********************
	 * MÉTODOS
	 *********************/
	@Override
	public String cadastrar() {
		// TODO Auto-generated method stub
		this.fornecedor = new Fornecedor();
		return URL_CADASTRO;
	}

	@Override
	public String editar() {
		// TODO Auto-generated method stub
		return URL_EDITAR;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return URL_LISTAR;
	}

	@Override
	public String alterar() {
		// TODO Auto-generated method stub
		try {
			doDao.atualizar(fornecedor);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					MensagensUtil.getMessagem("MN005"), MensagensUtil.getMessagem("MN005")));
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN003"), MensagensUtil.getMessagem("MN003")));
		}
		return null;
	}

	@Override
	public String excluir() {
		// TODO Auto-generated method stub
		try {
			doDao.remove(fornecedor);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					MensagensUtil.getMessagem("MN007"), MensagensUtil.getMessagem("MN004")));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN003"), MensagensUtil.getMessagem("MN003")));
		}
		return null;
	}

	@Override
	public String salvar() {
		// TODO Auto-generated method stub
		try {
			doDao.gravar(fornecedor);
			fornecedor = new Fornecedor();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					MensagensUtil.getMessagem("MN002"), MensagensUtil.getMessagem("MN003")));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN003"), MensagensUtil.getMessagem("MN003")));
		}
		return null;
	}

	@Override
	public String pesquisar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visualizar() {
		// TODO Auto-generated method stub
		return null;
	}

	/************************
	 * Getters e setters
	 *************************/
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public List<Fornecedor> getFornecedores() {
		setFornecedores(doDao.listaTodos());
		return fornecedores;
	}

	public void setFornecedores(List<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}

	

}
