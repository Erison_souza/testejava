package br.open.system.teste.java.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "cliente", schema = "testejava")
@PrimaryKeyJoinColumn(name = "idPessoa")
@Inheritance(strategy = InheritanceType.JOINED)
@XmlRootElement
public class Cliente extends Master{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5911908513734342376L;
	
	@Column(name="cpf")
	private String cpf;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	
}
