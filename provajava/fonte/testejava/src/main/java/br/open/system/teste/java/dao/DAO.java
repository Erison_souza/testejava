package br.open.system.teste.java.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import br.open.system.teste.java.conection.ConectionJPA;

public class DAO<T> {

	private final Class<T> classe;

	private EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public DAO(Class<T> classe) {
		this.classe = classe;
	}

	/**************************************************
	 * Esse método serve para gravar uma Entidade em banco de dados
	 * 
	 * @param t
	 * @return true a operação seja realizada com sucesso. False, caso
	 *         contrário.
	 ********************************************************/
	public boolean gravar(T t) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		try {

			// abre transacao

			entityManager.getTransaction().begin();
			// persiste o objeto
			entityManager.persist(t);
			// commita a transacao
			entityManager.getTransaction().commit();
			// fecha a entity manager
			entityManager.close();
			return true;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			return false;
		}
	}

	/***********************************************************
	 * Esse me´todo serve para deletar uma entidade do banco de dados
	 * 
	 * @param t
	 * @return true a operação seja realizada com sucesso. False, caso
	 *         contrário.
	 ********************************************************/

	public boolean remove(T t) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();

		try {
			entityManager.getTransaction().begin();

			entityManager.remove(entityManager.merge(t));

			entityManager.getTransaction().commit();
			entityManager.close();
			return true;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			return false;
		}
	}

	/**
	 * Esse método serve para atualizar uma Entidade
	 * 
	 * @param t
	 * @return true a operação seja realizada com sucesso. False, caso
	 *         contrário.
	 */
	public boolean atualizar(T t) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		try {
			entityManager.getTransaction().begin();

			entityManager.merge(t);

			entityManager.getTransaction().commit();
			entityManager.close();
			return true;
		} catch (Exception e) {
			entityManager.getTransaction().rollback();
			return false;
		}
	}

	/**
	 * Esse método serve listar todos os resustados de uma entidade.
	 * 
	 * @return lista de todas as entidades
	 */
	public List<T> listaTodos() {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		CriteriaQuery<T> query = entityManager.getCriteriaBuilder().createQuery(classe);
		query.select(query.from(classe));

		List<T> lista = entityManager.createQuery(query).getResultList();

		entityManager.close();
		return lista;
	}

	/**
	 * Esse método serve salvar ou atualizar uma entidade no banco de dados.
	 * 
	 * @param id
	 * @return enditade procurada
	 */
	public T buscaPorId(Integer id) {
		EntityManager entityManager = new ConectionJPA().getEntityManager();
		T instancia = entityManager.find(classe, id);
		entityManager.close();
		return instancia;
	}

	/**
	 * Esse métod serve para salvar uma entidade.
	 * 
	 * @param t
	 * @return entidade
	 */
	public T salvar(T t, Integer id) {

		EntityManager entityManager = new ConectionJPA().getEntityManager();
		try {
			// Inicia uma transação com o banco de dados.
			entityManager.getTransaction().begin();

			// Verifica se a entidade ainda não está salva no banco de dados.

			if (id == null) {
				// Salva os dados da entidade.
				entityManager.persist(t);
			} else {
				// Atualiza os dados da entidade.
				t = entityManager.merge(t);
			}
			// Finaliza a transação.
			entityManager.getTransaction().commit();

		} finally {
			entityManager.close();
		}
		return t;

	}

}
