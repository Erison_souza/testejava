package br.open.system.teste.java.entidade;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

//http://www.devmedia.com.br/tipos-de-heranca-no-hibernate/28641

/**
 * Classe pessoa. 
 * Essa classe é a classe pai dos relacionamento.
 * @author erison
 *
 */
@Entity
@Table(name = "master", schema = "testejava")
@Inheritance(strategy = InheritanceType.JOINED)
@XmlRootElement
public class Master implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5429666085146721829L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_pessoa")
	private Integer idPessoa;
	@Column(name="nome")
	private String nome;
	@Column(name="endereco")
	private String endereco;
	@Column(name="telefone")
	private String telefone;
	
	
	// getters and setters 
	public Integer getIdPessoa() {
		return idPessoa;
	}
	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	
}
