package br.open.system.teste.java.mb;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.open.system.teste.java.dao.DAO;
import br.open.system.teste.java.entidade.Cliente;
import br.open.system.teste.java.entidade.Fornecedor;
import br.open.system.teste.java.util.MensagensUtil;

/**
 * Essa classe é a classe controler das telas de cliente.
 * 
 * @author erison
 *
 */
@ManagedBean
@SessionScoped
public class ClienteMB implements InterfaceMB {

	/**
	 * DEFINIÇÃO DAS URLS DA PÁGINA
	 */
	private static String URL_CADASTRO = "/pages/cliente/mvc/new-client.xhtml?faces-redirect=true";
	private static String URL_LISTAR = "/pages/cliente/mvc/list-clients.xhtml?faces-redirect=true";
	private static String URL_EDITAR = "/pages/cliente/mvc/edit-client.xhtml?faces-redirect=true";
	/***********************
	 * VARIÁVEIS
	 **********************/

	private Cliente cliente;
	private List<Cliente> clientes;
	private DAO<Cliente> doDao;

	public ClienteMB() {
		super();
		doDao = new DAO<>(Cliente.class);
		this.cliente = new Cliente();
		clientes = new ArrayList<>();
	}
	
	@Override
	public String cadastrar() {
		// TODO Auto-generated method stub
		this.cliente = new Cliente();
		doDao = new DAO<>(Cliente.class);
		return URL_CADASTRO;
	}

	@Override
	public String listar() {
		// TODO Auto-generated method stub
		return URL_LISTAR;
	}

	@Override
	public String editar() {
		// TODO Auto-generated method stub
		return URL_EDITAR;
	}

	@Override
	public String alterar() {
		// TODO Auto-generated method stub
		try {
			doDao.atualizar(cliente);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					MensagensUtil.getMessagem("MN004"), MensagensUtil.getMessagem("MN004")));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN003"), MensagensUtil.getMessagem("MN003")));
		}
		return null;
	}

	@Override
	public String excluir() {
		// TODO Auto-generated method stub
		try {
			doDao.remove(cliente);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					MensagensUtil.getMessagem("MN007"), MensagensUtil.getMessagem("MN004")));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN003"), MensagensUtil.getMessagem("MN003")));
		}
		return null;
	}

	@Override
	public String salvar() {
		// TODO Auto-generated method stub
		try {
			doDao.gravar(cliente);
			this.cliente = new Cliente();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					MensagensUtil.getMessagem("MN001"), MensagensUtil.getMessagem("MN001")));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MensagensUtil.getMessagem("MN003"), MensagensUtil.getMessagem("MN003")));
		}
		return null;
	}

	@Override
	public String pesquisar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String visualizar() {
		// TODO Auto-generated method stub
		return null;
	}
	/************************
	 * Getters e setters
	 *************************/
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Cliente> getClientes() {
		setClientes(doDao.listaTodos());
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

}
