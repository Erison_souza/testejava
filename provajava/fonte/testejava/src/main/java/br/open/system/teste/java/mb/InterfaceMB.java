package br.open.system.teste.java.mb;

public interface InterfaceMB {
	
	/**************************************
	 * Chama a tela de cadastro
	 * @return url da tela de cadastro
	 **************************************/
	public String cadastrar();
	/**************************************
	 * Lista 
	 * @return url da tela de cadastro
	 **************************************/
	public String listar();
	/**************************************
	 * Altera entidade
	 * @return url da página
	 **************************************/
	public String alterar();
	/**************************************
	 * Exclui entidade
	 * @return url página
	 **************************************/
	public String excluir();
	/**************************************
	 * Chama salva a entidade em banco de dados
	 * @return url da página
	 **************************************/
	public String salvar();
	/**************************************
	 * Chama Pesquisa entidaade
	 * @return url página
	 **************************************/
	public String pesquisar();
	/**************************************
	 * Chama visualiza dados da entidade
	 * @return url página
	 **************************************/
	public String visualizar();
	/**************************************
	 * Chama a tela de alterar
	 * @return url página de visualizar
	 **************************************/
	public String editar();

}
